<?php
declare (strict_types = 1);

namespace app\middleware;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use think\Exception;

class Permissions
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {

        $jwt = $request->param('token');

        $key='zbc';
        JWT::$leeway = 60;
        $decoded = JWT::decode($jwt, new Key($key, 'HS256'));
        if($decoded){
            return $next($request);
        }



    }
}
