<?php
/**
 * Created by PhpStorm.
 * User: 10037
 * Date: 2022/6/27
 * Time: 20:15
 */

namespace app\admin\model;

use think\model;

class Questionbanktable extends model
{
    protected $name='questionbanktable';
    protected $pk='id';

    public function selectIdBatch($id){
        return $this->find($id);
    }
}