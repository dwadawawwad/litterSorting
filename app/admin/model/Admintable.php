<?php
/**
 * Created by PhpStorm.
 * User: 10037
 * Date: 2022/6/27
 * Time: 20:13
 */

namespace app\admin\model;


use think\Model;

class Admintable extends Model
{
    protected $pk='id';

    public function  adminLogin($adminAccount,$adminPassword){
        $result= $this->where('adminAccount','=',$adminAccount)->find();
        if(!empty($result)) {
            if ($result['adminPassword'] === $adminPassword) {
                return jsonStatus(true, '登陆成功', $result);
            }
        }
        return jsonStatus(false,'登陆失败','');
    }
}