<?php
/**
 * Created by PhpStorm.
 * User: 10037
 * Date: 2022/6/27
 * Time: 18:48
 */

namespace app\admin\controller;

use app\admin\model\Questionbanktable;
use think\facade\Request;


class QuestionBankHome
{
    //答题页面
    public function QuestIndex(){
        //当前页为1 数量为：5
        $page=Request::param('page');
        $Rows=Request::param('Rows');
        //默认为 page=1 Rows=5
        if(empty($page)){
            $page=1;
        };
        if(empty($Rows)){
            $Rows=5;
        };
        if((int)$page==0){
            return json(jsonStatus(0,'传入的参数类型不对',''));
        }
        if((int)$page==0){
            return json(jsonStatus(0,'传入的参数类型不对',''));
        };
        $model=new Questionbanktable();
        $result=$model->where('delete','<>',0)->page($page,$Rows)->select();
        $count=$model->where('delete','<>',0)->page($page,$Rows)->count();
        return json(jsonStatus(1,'操作成功',[
            "row"=>$count,
            "data"=>$result
        ]));
    }
    //查询题目
    public function Select(){
        //按题目标题查询
        $TitleName=Request::post('topicTitle');
        //按题目内容查询
        $TextName=Request::post('topicText');
        $model=new Questionbanktable();
        $where=[];
        if (!empty($TitleName)){
            $where[] = ['topicTitle', 'like', "%$TitleName%"];
        }
        if (!empty($TextName)){
            $where[] = ['topicText', 'like', "%$TextName%"];
        }
        $where[] = ['delete', '<>', 0];
        $data=$model->where($where)->limit(10)->select()->toArray();
        if (empty($data)){
            return json(jsonStatus(0,'数据查无此题目信息',''));
        }
        return json(jsonStatus(1,'查询成功',$data));
    }
    //增加题目
    public function Insert(){
        //获取新增题目的信息(确定全部传入的值 有值)
        $topicTitle=Request::post('topicTitle');
        $topicText=Request::post('topicText');
        $topicAnswer=Request::post('topicAnswer');
        $topicOptions=Request::post('topicOptions');
        if((int)$topicAnswer==0){
            return json(jsonStatus(0,'传入的参数类型不对',''));
        }
        $model=new Questionbanktable();
        //先查询一下是否已经有完全相等的题目内容存在
        $FindData=$model->where("topicText","=",$topicText)->find();
        if (!empty($FindData)){
            return json(jsonStatus(0,'该数据已存在',''));
        }
        $data= [
            "topicTitle"=>$topicTitle,
            "topicText"=>$topicText,
            "topicAnswer"=>$topicAnswer,
            "topicOptions"=>$topicOptions
        ];
        $status=$model->save($data);
        if (!$status){
            return json(jsonStatus(0,'新增失败！！！',''));
        }
        return json(jsonStatus(1,'新增成功！！！',''));
    }
    //修改题目
    public function Update(){
        //获取要修改的题目标题
        $topicTitle=Request::post('topicTitle');
        $topicText=Request::post('topicText');
        $topicAnswer=Request::post('topicAnswer');
        $topicOptions=Request::post('topicOptions');
        if((int)$topicAnswer==0){
            return json(jsonStatus(0,'传入的参数类型不对',''));
        }
        $model=new Questionbanktable();
        $result=$model->where('topicTitle','=',$topicTitle)->find();
        if (empty($result)){
            return json(jsonStatus(0,'改题目不存在',''));
        }
        $result['topicText']=$topicText;
        $result['topicAnswer']=$topicAnswer;
        $result['topicOptions']=$topicOptions;
        if (!$result->save()){
            return json(jsonStatus(0,'修改失败',''));
        }
        return json(jsonStatus(1,'修改成功',''));
    }
    //删除题目
    //单个删除
    public function Delete(){
        //获取题目ID
        $id=Request::delete('id');
        if (empty($id)){
            return json(jsonStatus(0,'传入的ID为空',''));
        }
        //创建数据库实例
        $Model=new Questionbanktable();
        $where[] = [
            'id',
            '=',
            $id
        ];
        $result=$Model->where($where)->find();
        //对传入的值进行判断是否在数据库有数据
        if (empty($result)){
            return json(jsonStatus(0,'要删除的数据在数据库不存在',''));
        }
        if ($result['delete']==1){
            $result['delete']=0;
            $status=$result->save();
            if($status==1){
                return json(jsonStatus(1,'题目删除成功',''));
            }
        }
        else{
            return json(jsonStatus(0,'题目删除失败',''));
        }
    }
    //批量删除
    public function DeleteBatch(){
        $ids=Request::delete('ids');
        if (!is_array($ids)){
            return json(jsonStatus(0,'传入的不是数组',''));
        }
        //创建数据库实例
        $Model=new Questionbanktable();
        $where[] = ['id', 'in', $ids];
        $result=$Model->where($where)->select()->toArray();
        //对传入的值进行判断是否在数据库有数据
        if (empty($result)){
            return json(jsonStatus(0,'要删除的数据在数据库不存在',''));
        }
        for ($i=0;$i<count($ids);$i++) {
            if ($result[$i]['delete'] == 0) {
                return json(jsonStatus(0,'批量删除失败',''));
            }
        }
        $num=0;
        for ($i=0;$i<count($ids);$i++){
            if ($result[$i]['delete']==0){

            }
            if ($result[$i]['delete']!=0){
                $Model->update(['delete'=>0],['id'=>$ids[$i]]);
                $num++;
            }
        }
        if ($num!=count($ids)){
            return json(jsonStatus(0,'批量删除失败',''));
        }
        return json(jsonStatus(1,'批量删除成功',''));
    }

    public function selectIdBatch($id){
        $quest=new Questionbanktable();
        $data= $quest->selectIdBatch($id);
        return jsonStatus(true,'查询成功',$data);
    }
}