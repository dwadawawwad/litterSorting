<?php
/**
 * Created by PhpStorm.
 * User: 10037
 * Date: 2022/6/30
 * Time: 8:42
 */

namespace app\admin\controller;


use app\admin\model\Admintable;
use Firebase\JWT\JWT;
use jolalau\captcha\facade\CaptchaApi;

class LoginHome
{
    public function adminLogin($acount,$password){
        $admin=new Admintable();
        $result= $admin->adminLogin($acount,$password);
        if($result['status']===200){
            $key = 'zbc';
            $payload = [
                'id'=>$result['data']['id'],
                'username'=>$result['data']['adminName']
            ];

            $jwt = JWT::encode($payload, $key, 'HS256');

            return json(jsonStatus(true,'登陆成功',['token'=>$jwt,'adminImage'=>$result['data']['adminImage'],'adminName'=>$result['data']['adminName']]));
        }

        return json(jsonStatus(false,'登陆失败',''));

    }

    public function version(){
        $data=CaptchaApi::create();
        return json(jsonStatus(true,'验证码请求成功',['baseUrl'=>$data['base64'],'key'=>$data['key']]));
    }

    public function codeVersion($code,$key){
        if(!CaptchaApi::check($code,$key)){
            return jsonStatus(false,'验证码错误','');
        }

        return jsonStatus(true,'验证码正确','');
    }
}