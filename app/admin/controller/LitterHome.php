<?php
/**
 * Created by PhpStorm.
 * User: 10037
 * Date: 2022/6/27
 * Time: 18:46
 */

namespace app\admin\controller;


use app\admin\model\Sortingtable;
use app\admin\model\Spamtable;
use app\BaseController;

class LitterHome extends BaseController
{

    //参数返回
    public function getClassifcation(){
        $data=1;
       return json(jsonStatus(false,'我草泥马',$data));
    }
    //单删除
    public function classifDelete(){
        //请求获取所删除类别id
        $ids=$this->request->post('id');
        $id=(integer)$ids;
        //判断变量类型是否符合
//        if(!gettype($id)=='integer'){
//            return json([
//                'status'=>400,
//                'message'=>'传入的参数类型与数据库不匹配'
//            ]);
//        }
        //判断该类别中是否还有垃圾
        $model=new Spamtable();
        $result=$model->where([['rubbishTypeId','=',$id]])->find();

        if(!empty($result)){
            return json(jsonStatus(false,'此类别中还有数据，不可删除',''));
        }
        //调用删除工具进行软删除
        return $this->delTool($id);
    }
    //全选删除
    public function classifAllDel(){
        //定义数组变量  接收请求的id
        $ids=$this->request->post('id');
        
        //判断ids是否有请求到值
        if(empty($ids)){
            return json(jsonStatus(false,'id值有误',''));

        }

        //遍历删除id对应类别
        $model=new Spamtable();
        $modeltwo=new Sortingtable();
        foreach ($ids as $item){
            //判断获取的值类型是否正确
//                if(!gettype($item)=='integer'){
//                    return json([
//                       'status'=>400,
//                       'message'=>'传入的参数类型与数据库不匹配'
//                    ]);
//                }
            //用类别id到垃圾信息表匹配数据
            $record=$model->where([['rubbishTypeId','=',$item]])->find();
            //对应类别id的垃圾类别信息
            $result=$modeltwo->where([['id','=',$item]])->find();
            //如果垃圾信息表匹配数据不为空代表该类别中还有数据 不可删除
            if(!empty($record)){
                return json(jsonStatus(false,$result['sortingName'].'该类别中还有垃圾数据',''));

            }
            //至此代表类别中没有垃圾数据  则进行删除
            $result['delete']=0;
            //失败返回json
            if(!$result->save()){
                return json(jsonStatus(false,'删除更新失败',''));

            }
        }

        //循环结束 删除完毕 返回json
        return json(jsonStatus(true,'批量删除成功',''));

    }

    //删除工具
    public function delTool($id){
        if($id==0){

            return json(jsonStatus(false,'id值有误',''));
        }
        $model=new Sortingtable();
        $result=$model->where([['id','=',$id]])->update(['delete'=>0]);
//        if($result==0){
//
//            return json([
//                'status'=>400,
//                'message'=>'此类别已处于删除状态',
//                'data'=>'',
//            ]);
//        }
        return json(jsonStatus(true,'删除成功',''));


    }
    //添加类别

    public function classifAdd(){

        $sorting=$this->request->post('sortingName');
        $sortingName=(string)$sorting;
        $model=new Sortingtable();
        $where=[];
        array_push($where,[
            'sortingName',
            '=',
            $sortingName
        ]);
        $result=$model->where($where)->find();

        if(!empty($result)){
            return json(jsonStatus(false,'此类别已存在',''));

        }
        $add_yes=$model->save(['sortingName'=>$sortingName]);
        if(!$add_yes){
            return json(jsonStatus(true,'新增失败',''));

        }
        return json(jsonStatus(true,'新增成功',''));

    }
    //修改
    public function classifEdit(){
        $ids=$this->request->post('id');
        $sortingName=$this->request->post('sortingName');
        $id=(integer)$ids;
        $model=new Sortingtable();
        $record=$model->where([['sortingName','=',$sortingName]])->find();
        if(!empty($record)){
            return json(jsonStatus(false,'此类别已存在',''));

        }
//        $status=$this->request->post('status');
//        $delete=$this->request->post('delete');

        $where=[];
        array_push($where,[
            'id',
            '=',
            $id
        ]);
        $result=$model->where($where)->find();

        $result['sortingName']=$sortingName;
//        $result['status']=$status;
//        $result['delete']=$delete;

        if(!$result->save()){
            return json(jsonStatus(false,'更新失败',''));

        }
        return json(jsonStatus(true,'更新成功',''));

    }
    ////////////////////////////////////////////
    //spamtable(垃圾信息)
    //垃圾添加
    public function rbSpamAdd(){
        //获取垃圾名称
        $rb_id=$this->request->post('rubbishTypeId');
        $rb_name=$this->request->post('rubbishName');
        $model=new Spamtable();
        $result=$model->where([['rubbishName','=',$rb_name]])->find();

        if(!empty($result)){
            return json(jsonStatus(false,'此垃圾已存在,无需添加',''));

        }

        $result=$model->save([
            'rubbishTypeId'=>$rb_id,
            'rubbishName'=>$rb_name,
            'createdTime'=> date('Y-m-d H-i-s'),
            'updatedTime'=> date('Y-m-d H-i-s')
        ]);

        if(!$result){
            return json(jsonStatus(false,'新增失败',''));

        }
        return json(jsonStatus(true,'新增成功',''));


    }
    //垃圾类别返回
    public function returnSpam(){
        //获取数据库类别
        $model=new Sortingtable();
        $result=$model->select();
        return json(jsonStatus(true,'类别返回成功',$result->toArray()));

    }
}
