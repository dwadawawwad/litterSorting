<?php
/**
 * Created by PhpStorm.
 * User: 10037
 * Date: 2022/6/27
 * Time: 20:10
 */
use think\facade\Route;

//题目页面
Route::rule('QuestIndex','QuestionBankHome/QuestIndex','POST');
//查询题目
Route::rule('QuestSelect','QuestionBankHome/Select','POST');
//新增题目
Route::rule('QuestInsert','QuestionBankHome/Insert','POST');
//修改提米
Route::rule('QuestUpdate','QuestionBankHome/Update','POST');
//删除路由
Route::rule('QuestDelete','QuestionBankHome/Delete','DELETE');
//批量删除路由

Route::post('selectIdBatch','QuestionBankHome/selectIdBatch');
//单个查询

Route::rule('QuestDeBatch','QuestionBankHome/DeleteBatch','GEt|DELETE');

Route::delete('QuestDeBatch','QuestionBankHome/DeleteBatch');
//登录
Route::post('adminLogin','LoginHome/adminLogin');


//类别删除
Route::rule('litterdel','LitterHome/classifDelete');

Route::rule('litterdelall','LitterHome/classifAllDel');
//类别添加
Route::rule('litteradd','LitterHome/classifAdd');
//类别修改
Route::rule('litteredit','LitterHome/classifEdit');
//垃圾添加
Route::rule('spamadd','LitterHome/rbSpamAdd');
//类别返回
Route::rule('spamreturn','LitterHome/returnSpam');

//生成验证码
Route::get('version','LoginHome/version');
//验证验证码
Route::post('codeVersion','LoginHome/codeVersion');
