<?php
// 应用公共文件
function jsonStatus($result,$message,$data){
    if($result){
        return [
            'status'=>200,
            'message'=>$message,
            'data'=>$data
        ];
    }

    return [
        'status'=>400,
        'message'=>$message,
        'data'=>$data
    ];
}