/*
 Navicat Premium Data Transfer

 Source Server         : zbc
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : garbagesorting

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 27/06/2022 20:27:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tp_admintable
-- ----------------------------
DROP TABLE IF EXISTS `tp_admintable`;
CREATE TABLE `tp_admintable`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理员id',
  `adminName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员姓名',
  `adminAccount` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员账号',
  `adminPassword` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员密码',
  `adminImage` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员头像',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tp_admintable
-- ----------------------------

-- ----------------------------
-- Table structure for tp_informationtable
-- ----------------------------
DROP TABLE IF EXISTS `tp_informationtable`;
CREATE TABLE `tp_informationtable`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '资讯id',
  `informationTitle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资讯标题',
  `informationText` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资讯内容',
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否勾选：1未勾选 0勾选',
  `delete` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否删除：1未删除 0删除',
  `addTime` datetime NOT NULL COMMENT '添加时间',
  `updateTime` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tp_informationtable
-- ----------------------------

-- ----------------------------
-- Table structure for tp_logtable
-- ----------------------------
DROP TABLE IF EXISTS `tp_logtable`;
CREATE TABLE `tp_logtable`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '日志id',
  `logText` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '操作内容',
  `logTime` datetime NOT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tp_logtable
-- ----------------------------

-- ----------------------------
-- Table structure for tp_messagetable
-- ----------------------------
DROP TABLE IF EXISTS `tp_messagetable`;
CREATE TABLE `tp_messagetable`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '留言id',
  `userid` int(11) NOT NULL COMMENT '关联用户id',
  `messageText` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '留言内容',
  `messageTime` datetime NOT NULL COMMENT '留言时间',
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否勾选 1未勾选 0勾选',
  `delete` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否删除 1未删除 0删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `userFk`(`userid`) USING BTREE,
  CONSTRAINT `userFk` FOREIGN KEY (`userid`) REFERENCES `tp_usertable` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tp_messagetable
-- ----------------------------

-- ----------------------------
-- Table structure for tp_questionbanktable
-- ----------------------------
DROP TABLE IF EXISTS `tp_questionbanktable`;
CREATE TABLE `tp_questionbanktable`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '题目id',
  `topicTitle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '题目标题',
  `topicText` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '题目内容',
  `topicAnswer` int(11) NOT NULL COMMENT '题目答案',
  `topicOptions` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '题目选项  xxx|xxx|xxx|xxx| 间隔格式',
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否勾选 1未勾选 0勾选',
  `delete` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否删除 1为删除 0删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tp_questionbanktable
-- ----------------------------

-- ----------------------------
-- Table structure for tp_sortingtable
-- ----------------------------
DROP TABLE IF EXISTS `tp_sortingtable`;
CREATE TABLE `tp_sortingtable`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '垃圾分类id',
  `sortingName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类名称',
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否勾选 1未勾选 0勾选',
  `delete` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否删除 1未删除 0删除 ',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tp_sortingtable
-- ----------------------------

-- ----------------------------
-- Table structure for tp_spamtable
-- ----------------------------
DROP TABLE IF EXISTS `tp_spamtable`;
CREATE TABLE `tp_spamtable`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '垃圾Id',
  `rubbishName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '垃圾名称',
  `rubbishTypeId` int(11) NOT NULL COMMENT '垃圾类别',
  `delete` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否删除？1未删除 0已删除',
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否勾选？1未勾选 0已勾选',
  `createdTime` datetime NOT NULL COMMENT '创建时间',
  `updatedTime` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sortingFk`(`rubbishTypeId`) USING BTREE,
  CONSTRAINT `sortingFk` FOREIGN KEY (`rubbishTypeId`) REFERENCES `tp_sortingtable` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tp_spamtable
-- ----------------------------

-- ----------------------------
-- Table structure for tp_usertable
-- ----------------------------
DROP TABLE IF EXISTS `tp_usertable`;
CREATE TABLE `tp_usertable`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `userPhone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户手机号',
  `delete` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否删除',
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否勾选',
  `integral` int(11) NOT NULL DEFAULT 0 COMMENT '积分',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户默认地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tp_usertable
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
